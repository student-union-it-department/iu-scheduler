# iu-scheduler



## How to run?

Run following bash scripts to run the server application:
0. Create .env file withing the project directory

1. Built the project:
```
sbt docker:publishLocal
```

Once app is loaded, run second step to run the app:
```
docker-compose up -d --build
```
then
```
docker-compose logs -f
```