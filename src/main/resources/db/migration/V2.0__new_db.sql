
create table if not exists "timetablee" (
    "id" bigserial primary key,
    "group" varchar,
    "study" varchar,
    "sub_group_table" varchar
);