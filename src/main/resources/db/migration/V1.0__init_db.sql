
CREATE TABLE IF NOT EXISTS AccessLevel (

  Access_id INT PRIMARY KEY,
  Access_Name VARCHAR (250) NOT NULL,
  URL_Link VARCHAR(750) NOT NULL
  
);

CREATE TABLE IF NOT EXISTS LOGIN (

  Login_id INT PRIMARY KEY,
  Username VARCHAR (250) NOT NULL,
  Password VARCHAR (250) NOT NULL,
  AccessLevelVal VARCHAR(250) NOT NULL,
  Access_id INT NOT NULL,

  FOREIGN KEY (Access_id) REFERENCES AccessLevel (Access_id)
  
);

CREATE TABLE IF NOT EXISTS Groups (

  Group_id INT PRIMARY KEY,
  Group_Name VARCHAR (250) NULL

);


CREATE TABLE IF NOT EXISTS Department (
  Department_id INT PRIMARY KEY,
  Department_Name VARCHAR (250) NULL,
  Group_id INT NOT NULL,

  FOREIGN KEY (Group_id) REFERENCES Groups(Group_id)

);



CREATE TABLE IF NOT EXISTS Courses (

  Course_id INT PRIMARY KEY,
  Course_Name VARCHAR(255) NULL,
  Course_Credits INT NULL,
  Course_Code VARCHAR(50),
  Course_Type VARCHAR(50),
  Lab BIT NULL,
  Duration INT,
  Department_id INT NOT NULL,
  Group_id INT NOT NULL,
  FOREIGN KEY (Department_id) REFERENCES Department(Department_id),
  FOREIGN KEY (Group_id) REFERENCES Groups(Group_id)

);


CREATE TABLE IF NOT EXISTS Faculty (

  Faculty_id INT PRIMARY KEY,
  Faculty_Name VARCHAR (255) NULL,
  Faculty_email VARCHAR (255) NULL,
  Department_id INT NOT NULL,
  ----------Foreignkey_Stuff-----------
  FOREIGN KEY (Department_id) REFERENCES Department(Department_id)
  
);

CREATE TABLE IF NOT EXISTS Room (

  Room_id INT PRIMARY KEY,
  Room_Code VARCHAR(100),
  Room_Capacity INT NULL,
  Department_id INT NOT NULL,
  FOREIGN KEY (Department_id) REFERENCES Department(Department_id)
    
);


CREATE TABLE IF NOT EXISTS Schedules (

  Schedules_id INT PRIMARY KEY,
  Schedules_StartTime TIME,
  Schedules_EndTime TIME,
  Schedules_Day VARCHAR(300),
  Department_id INT NOT NULL,
  Course_id INT NOT NULL,

  FOREIGN KEY (Department_id) REFERENCES Department(Department_id),
  FOREIGN KEY (Course_id) REFERENCES Courses(Course_id)
   
);

CREATE TABLE IF NOT EXISTS TimeTable (
	
  	tt_id INT NOT NULL,
  	tt_CourseName VARCHAR(255) NULL,
  	tt_DepartmentName VARCHAR(255) NULL,
  	tt_RoomCode VARCHAR(100) NULL,
  	tt_Day VARCHAR(255) NULL,
  	tt_StartTime TIME NULL,
  	tt_EndTime TIME NULL,
  
  	Department_id INT NOT NULL,
  	Course_id INT NOT NULL,
	Room_id INT NOT NULL,
	Faculty_id INT NOT NULL,
	Schedules_id INT NOT NULL,
	Group_id INT NOT NULL,

  	FOREIGN KEY (Room_id) REFERENCES Room(Room_id),
  	FOREIGN KEY (Course_id) REFERENCES Courses(Course_id),
  	FOREIGN KEY (Department_id) REFERENCES Department(Department_id),
  	FOREIGN KEY (Faculty_id) REFERENCES Faculty(Faculty_id),
  	FOREIGN KEY (Schedules_id) REFERENCES Schedules(Schedules_id),
    FOREIGN KEY (Group_id) REFERENCES Groups(Group_id)
  	
);

CREATE TABLE IF NOT EXISTS Const (

  Schedules_id INT PRIMARY KEY,
  Schedules_StartTime TIMESTAMP,
  Schedules_EndTime TIMESTAMP

);


