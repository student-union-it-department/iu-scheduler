import cats.effect.{ExitCode, IO, IOApp, Resource}
import com.comcast.ip4s.IpLiteralSyntax
import com.typesafe.config.ConfigFactory
import doobie.util.ExecutionContexts
import iu.scheduler.config.AppConfig
import iu.scheduler.controllers.UniversalController
import iu.scheduler.db.DbConfig
import iu.scheduler.domain.{Swagger, Version}
import iu.scheduler.domain.accesslvl.{AccessLevel, AccessLevelService}
import iu.scheduler.domain.courses.{Course, CourseService}
import iu.scheduler.domain.faculty.{Faculty, FacultyService}
import iu.scheduler.domain.login.{Login, LoginService}
import iu.scheduler.domain.room.{Room, RoomService}
import iu.scheduler.domain.schedule.{Schedule, ScheduleService}
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import cats.syntax.all._
import iu.scheduler.domain.v2.timetable.{TTService, TimeTable}
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.server.middleware.Logger
import sttp.apispec.openapi.circe.yaml.RichOpenAPI
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.server.http4s.{Http4sServerInterpreter, Http4sServerOptions}

import scala.collection.concurrent.TrieMap

object Main extends IOApp {

  def server: Resource[IO, Server] = for {
    config <- Resource.eval(IO(ConfigFactory.load()))
    conf <- Resource.eval(IO(AppConfig.fromConfig(config)))
    //    serverEc
    _ <- ExecutionContexts.cachedThreadPool[IO]
    connEc <-
      ExecutionContexts.fixedThreadPool[IO](conf.db.connections.poolSize)
    _ <- ExecutionContexts.cachedThreadPool[IO]
    xa <- DbConfig.dbTransactor[IO](
      conf.db,
      connEc
    )
    trie <- Resource.eval(IO(TrieMap.empty[Long, TimeTable]))

    client <- EmberClientBuilder
      .default[IO]
      .build

    accessLvlService <- Resource.pure(new AccessLevelService[IO](xa.trans))
    coursesService <- Resource.pure(new CourseService[IO](xa.trans))
    facultyService <- Resource.pure(new FacultyService[IO](xa.trans))
    loginService <- Resource.pure(new LoginService[IO](xa.trans))
    roomService <- Resource.pure(new RoomService[IO](xa.trans))
    scheduleService <- Resource.pure(new ScheduleService[IO](xa.trans))
    ttService <- Resource.pure(new TTService[IO](xa, trie, client))

    accessLvlEndpoint <- Resource.pure(
      new UniversalController[IO, AccessLevel](
        accessLvlService,
        "access-level"
      )
    )
    coursesEndpoint <- Resource.pure(
      new UniversalController[IO, Course](coursesService, "courses")
    )
    facultyEndpoint <- Resource.pure(
      new UniversalController[IO, Faculty](facultyService, "faculty")
    )
    loginEndpoint <- Resource.pure(
      new UniversalController[IO, Login](loginService, "login")
    )
    roomEndpoint <- Resource.pure(
      new UniversalController[IO, Room](roomService, "room")
    )
    scheduleEndpoint <- Resource.pure(
      new UniversalController[IO, Schedule](scheduleService, "schedule")
    )

    ttEndpoint <- Resource.pure(
      new UniversalController[IO, TimeTable](ttService, "time-table")
    )
    endpoints =
      (accessLvlEndpoint.services ++
        coursesEndpoint.services ++
        facultyEndpoint.services ++
        loginEndpoint.services ++
        roomEndpoint.services ++
        ttEndpoint.services ++
        scheduleEndpoint.services).toList

    yaml = OpenAPIDocsInterpreter()
      .toOpenAPI(endpoints.map(_.endpoint), "test", "v1")
      .toYaml

    routes <- Resource.pure(
      Http4sServerInterpreter[IO](Http4sServerOptions.default[IO])
        .toRoutes(endpoints) <+> Version.helloWorldRoutes[IO] <+> Swagger
        .swaggerRoutes[IO](yaml)
    )
    _ <- Resource.eval(DbConfig.initDb[IO](conf.db))

    app = (routes).orNotFound

    finalHttpApp = Logger.httpApp(true, true)(app)

    s <- EmberServerBuilder
      .default[IO]
      .withHttpApp(finalHttpApp)
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8080")
      .build

  } yield s

  override def run(args: List[String]): IO[ExitCode] = {
    server
      .use { w =>
        println(w.isSecure)
        IO.never
      }
      .as(ExitCode.Success)
  }
}
