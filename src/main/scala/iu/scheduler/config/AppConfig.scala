package iu.scheduler.config

import com.typesafe.config.Config
import iu.scheduler.db.DbConfig
import pureconfig.ConfigSource
import pureconfig.generic.auto._

final case class AppConfig(db: DbConfig, server: ServerConfig)
object AppConfig {
  def fromConfig(config: Config): AppConfig =
    ConfigSource.fromConfig(config).loadOrThrow[AppConfig]

}
