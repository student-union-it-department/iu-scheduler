package iu.scheduler.config

final case class ServerConfig(host: String, port: Int)
