package iu.scheduler.db

import cats.effect.{Async, Resource, Sync}
import cats.syntax.functor._
import doobie.hikari.HikariTransactor
import org.flywaydb.core.Flyway

import scala.concurrent.ExecutionContext

case class DatabaseConnectionsConfig(poolSize: Int)

case class DbConfig(
    url: String,
    driver: String,
    user: String,
    password: String,
    connections: DatabaseConnectionsConfig
)

object DbConfig {
  def dbTransactor[F[_]: Async](
      dbc: DbConfig,
      connEc: ExecutionContext
  ): Resource[F, HikariTransactor[F]] =
    HikariTransactor
      .newHikariTransactor[F](
        dbc.driver,
        dbc.url,
        dbc.user,
        dbc.password,
        connEc
      )
  def initDb[F[_]](config: DbConfig)(implicit s: Sync[F]): F[Unit] =
    s.delay {
      val flyWay: Flyway =
        Flyway
          .configure()
          .dataSource(config.url, config.user, config.password)
          .load()
      flyWay.repair()
      flyWay.migrate()
    }.as(())

}
