package iu.scheduler.utils

import tethys._

import java.time.format.DateTimeFormatter
import java.time._

trait timeInstances {
  implicit val instantWriter: JsonWriter[Instant] =
    JsonWriter.stringWriter.contramap(i =>
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
        i.atZone(ZoneId.systemDefault())
      )
    )

  implicit val instantReader: JsonReader[Instant] =
    JsonReader.stringReader.map(s =>
      Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(s))
    )

  implicit val localDateTimeReader: JsonReader[LocalDateTime] =
    JsonReader.stringReader.map(s =>
      LocalDateTime.from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(s))
    )

  implicit val localDateTimeWriter: JsonWriter[LocalDateTime] =
    JsonWriter.stringWriter.contramap(i =>
      DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(
        i.atZone(ZoneId.systemDefault())
      )
    )

  implicit val localDateWriter: JsonWriter[LocalDate] =
    JsonWriter.stringWriter.contramap(i =>
      DateTimeFormatter.ISO_LOCAL_DATE.format(i)
    )

  implicit val localDateReader: JsonReader[LocalDate] =
    JsonReader.stringReader.map(s =>
      LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(s))
    )

  implicit val localTimeReader: JsonReader[LocalTime] =
    JsonReader.stringReader.map(s =>
      LocalTime.from(DateTimeFormatter.ofPattern("H:mm").parse(s))
    )

  implicit val localTimeWriter: JsonWriter[LocalTime] =
    JsonWriter.stringWriter.contramap(i =>
      DateTimeFormatter.ofPattern("H:mm").format(i)
    )
}

object timeInstances extends timeInstances
