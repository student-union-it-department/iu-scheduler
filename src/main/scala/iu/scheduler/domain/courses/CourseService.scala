package iu.scheduler.domain.courses

import cats.implicits.{catsSyntaxApplicativeId, toFlatMapOps, toFunctorOps}
import cats.{Monad, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD

class CourseService[F[_]: Monad](tx: ConnectionIO ~> F)
    extends CRUD[F, Course] {
  import Course.dc._
  override def create(id: Long, data: Course): F[Unit] =
    tx(run(Course.queries.create(data))).void.flatTap(_ =>
      println(s"EntityCreate: $data").pure[F]
    )

  override def read(id: Long): F[Option[Course]] =
    tx(run(Course.queries.read(id)))
      .map(_.headOption)
      .flatTap(entity => println(s"Read entity: $entity").pure[F])

  override def update(id: Long, data: Course): F[Unit] =
    tx(run(Course.queries.update(data))).void.flatTap(_ =>
      println(s"Entity: $data updated").pure[F]
    )

  override def delete(id: Long): F[Unit] =
    tx(run(Course.queries.delete(id))).void
      .flatTap(_ => println(s"Courses entity under id=$id deleted").pure[F])
}
