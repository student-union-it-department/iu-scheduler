package iu.scheduler.domain.courses

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill.doobie.DoobieContext
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema
@derive(tethysReader, tethysWriter)
final case class Course(
    Course_id: Long,
    Course_Name: Option[String],
    Course_Credits: Option[Long],
    Course_Code: Option[String],
    Course_Type: Option[String],
    Lab: Boolean,
    Duration: Long,
    Department_id: Long
) extends CoreData

object Course {

  import io.getquill.{idiom => _, _}

  val dc = new DoobieContext.Postgres(SnakeCase) // Literal naming scheme
  import dc.{SqlInfixInterpolator => _, _}

  implicit val coursesSchema: Schema[Course] = Schema.derived
  val coursesDbSchema: Quoted[EntityQuery[Course]] = quote(
    querySchema[Course](
      "Courses",
      _.Course_id -> "Course_id",
      _.Course_Name -> "Course_Name",
      _.Course_Credits -> "Course_Credits",
      _.Course_Code -> "Course_Code",
      _.Course_Type -> "Course_Type",
      _.Lab -> "Lab",
      _.Duration -> "Duration",
      _.Department_id -> "Department_id"
    )
  )

  object queries {
    def create(course: Course): Quoted[Insert[Course]] =
      quote {
        coursesDbSchema.insertValue(lift(course))
      }
    def read(id: Long): Quoted[EntityQuery[Course]] =
      quote {
        coursesDbSchema.filter(_.Course_id == lift(id))
      }

    def update(course: Course): Quoted[Update[Course]] = quote(
      coursesDbSchema
        .filter(_.Course_id == lift(course.Course_id))
        .updateValue(lift(course))
    )

    def delete(id: Long): Quoted[Delete[Course]] = quote(
      coursesDbSchema
        .filter(_.Course_id == lift(id))
        .delete
    )
  }
}
