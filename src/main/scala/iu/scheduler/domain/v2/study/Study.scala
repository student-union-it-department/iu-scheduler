package iu.scheduler.domain.v2.study

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import iu.scheduler.domain.v2.period.{PeriodAssistant, PeriodInstructor}
import sttp.tapir.Schema

@derive(tethysReader, tethysWriter)
final case class Study(
    day: String,
    periods: List[PeriodInstructor]
)

object Study {
  implicit val studyISchema: Schema[Study] = Schema.derived
}

@derive(tethysReader, tethysWriter)
final case class StudyA(
    day: String,
    periods: List[PeriodAssistant]
)
object StudyA {
  implicit val studyASchema: Schema[StudyA] = Schema.derived
}
