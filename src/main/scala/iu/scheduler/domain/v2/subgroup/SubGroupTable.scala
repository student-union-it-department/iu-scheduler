package iu.scheduler.domain.v2.subgroup

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import iu.scheduler.domain.v2.study.StudyA
import sttp.tapir.Schema

@derive(tethysWriter, tethysReader)
final case class SubGroupTable(subgroup: String, study: List[StudyA])

object SubGroupTable {
  implicit val studySchema: Schema[SubGroupTable] = Schema.derived

}
