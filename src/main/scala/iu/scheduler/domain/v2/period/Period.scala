package iu.scheduler.domain.v2.period

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema
import iu.scheduler.utils.timeInstances._
import java.time.LocalTime

@derive(tethysReader, tethysWriter)
final case class PeriodInstructor(
    startTime: LocalTime,
    endTime: LocalTime,
    subject: String,
    numberOfStudents: Int,
    instructor: String,
    room: Int
) extends CoreData

object PeriodInstructor {
  implicit val periodInstructorSchema: Schema[PeriodInstructor] = Schema.derived

}

@derive(tethysReader, tethysWriter)
final case class PeriodAssistant(
    startTime: LocalTime,
    endTime: LocalTime,
    lab: String,
    numberOfStudents: Int,
    assistant: String,
    room: Int
) extends CoreData

object PeriodAssistant {
  implicit val periodAssistantSchema: Schema[PeriodAssistant] = Schema.derived
}
