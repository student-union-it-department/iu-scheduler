package iu.scheduler.domain.v2.timetable

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}

import iu.scheduler.domain.CoreData
import iu.scheduler.domain.v2.study.Study
import iu.scheduler.domain.v2.subgroup.SubGroupTable
import sttp.tapir.Schema

@derive(tethysReader, tethysWriter)
final case class TimeTable(
    id: Long,
    group: String,
    study: List[Study],
    subGroupTable: List[SubGroupTable]
) extends CoreData

@derive(tethysReader, tethysWriter)
final case class TimeTableRaw(
    id: Long,
    group: String,
    study: String,
    subGroupTable: String
)

object TimeTable {

  implicit val schemaTimetable: Schema[TimeTable] = Schema.derived

//
//  import doobie._
//  import doobie.implicits._
//  import tethys._
//  import tethys.jackson._
//  def create(id: Long, tt: TimeTable): Update0 = {
//    val jsonStudy = tt.study.asJson
//    val jsonSubGroup = tt.subGroupTable.asJson
//    println(s"$jsonStudy  $jsonSubGroup")
//    sql"""
//         INSERT INTO timetablee ("group", study, sub_group_table)
//         VALUES ('$id', '${tt.group}', '${jsonStudy}', '${jsonSubGroup}')
//         """.update
//  }
//
//  def read(id: Long): Query0[TimeTableRaw] = {
//
//    sql"""
//         SELECT id, "group", study, sub_group_table
//         FROM timetablee
//         WHERE id = $id
//         """.query[TimeTableRaw]
//  }
//
//  def delete(id: Long) = {
//
//    sql"""
//        DELETE FROM timetablee WHERE id = $id
//      """.update
//  }
//
//  def update(id: Long, tt: TimeTable) = {
//    val jsonStudy = tt.study.asJson
//    val jsonSubGroup = tt.subGroupTable.asJson
//    sql"""
//         UPDATE timetablee
//         SET id = ${tt.id} , "group" = ${tt.group}, study = ${jsonStudy}, sub_group_table = ${jsonSubGroup}
//         WHERE id = ${id}
//       """.update
//  }
}
//
//import cats.effect.{ExitCode, IO, IOApp}
//object kek extends IOApp {
//  import tethys.jackson._
//  import tethys._
//  override def run(args: List[String]): IO[ExitCode] = {
//    val json =
//      """
//        |{
//        |	"id": 1,
//        |	"group": "BS-Year-1",
//        |	"study": [
//        |		{
//        |			"day": "Monday",
//        |			"periods": [
//        |				{
//        |					"startTime": "09:00",
//        |					"endTime": "10:30",
//        |					"subject": "Lecture 1",
//        |					"numberOfStudents": 34,
//        |					"instructor": "instructor 1",
//        |					"room": 110
//        |				}
//        |			]
//        |		}
//        |	],
//        |	"subGroupTable": [
//        |		{
//        |			"subgroup": "B22-CS-01",
//        |			"study": [
//        |				{
//        |					"day": "Monday",
//        |					"periods": [
//        |						{
//        |							"startTime": "13:30",
//        |							"endTime": "15:00",
//        |							"lab": "lab 1",
//        |							"numberOfStudents": 34,
//        |							"assistant": "assistant 1",
//        |							"room": 110
//        |						}
//        |					]
//        |				}
//        |			]
//        |		}
//        |	]
//        |}
//        |""".stripMargin
//    println(json)
////    println(LocalTime.now())
//        println(json.jsonAs[TimeTable].toOption.get.asJson.strip())
//
//    //    println(LocalTime.parse("9:00"))
//    IO.pure(ExitCode.Success)
//  }
//}