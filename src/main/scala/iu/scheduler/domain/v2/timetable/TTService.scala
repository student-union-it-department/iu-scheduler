package iu.scheduler.domain.v2.timetable

import cats.effect.Concurrent
//import cats.effect.kernel.MonadCancelThrow
import doobie.util.transactor.Transactor
import iu.scheduler.domain.CRUD
import org.http4s.client.Client
import org.http4s.implicits.http4sLiteralsSyntax

import scala.collection.concurrent.TrieMap

final class TTService[F[_]: Concurrent](
    tx: Transactor[F],
    storage: TrieMap[Long, TimeTable],
    client: Client[F]
) extends CRUD[F, TimeTable] {
//  import cats.implicits.toFunctorOps
//  import doobie.implicits._
//  override def create(id: Long, data: TimeTable): F[Unit] =
//    TimeTable.create(id, data).run.transact(tx).void
//
//  override def read(id: Long): F[Option[TimeTable]] =
//    TimeTable
//      .read(id)
//      .option
//      .transact(tx)
//      .map(_.flatMap(ttRAw => ttRAw.asJson.jsonAs[TimeTable].toOption))
//
//  override def update(id: Long, data: TimeTable): F[Unit] =
//    TimeTable.update(id, data).run.transact(tx).void
//
//  override def delete(id: Long): F[Unit] =
//    TimeTable.delete(id).run.transact(tx).void

  import cats.Applicative
  import cats.syntax.all._
  import tethys._

  import tethys.jackson._

  override def create(id: Long, data: TimeTable): F[Unit] = {
    tx.kernel
    Applicative[F].pure(storage.put(id, data)).void
  }

  override def read(id: Long): F[Option[TimeTable]] = {
    Applicative[F].pure(storage.get(id)).flatMap {
      case Some(value) => value.some.pure
      case None =>
        client
          .expect[String](uri"scheduler:8090/schedule/genrate")
          .flatMap(_.jsonAs[TimeTable].toOption.pure)
    }
  }

  override def update(id: Long, data: TimeTable): F[Unit] =
    Applicative[F].pure(storage.update(id, data))

  override def delete(id: Long): F[Unit] =
    Applicative[F].pure(storage.remove(id)).void
}
