package iu.scheduler.domain.room

import cats.implicits.toFunctorOps
import cats.{Monad, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD

final class RoomService[F[_]: Monad](tx: ConnectionIO ~> F)
    extends CRUD[F, Room] {
  import Room.dc._
  override def create(id: Long, data: Room): F[Unit] =
    tx(run(Room.create(data))).void

  override def read(id: Long): F[Option[Room]] =
    tx(run(Room.read(id))).map(_.headOption)

  override def update(id: Long, data: Room): F[Unit] =
    tx(run(Room.update(data))).void

  override def delete(id: Long): F[Unit] =
    tx(run(Room.delete(id))).void
}
