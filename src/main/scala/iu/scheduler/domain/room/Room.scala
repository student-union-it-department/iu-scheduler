package iu.scheduler.domain.room

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema
@derive(tethysReader, tethysWriter)
final case class Room(
    Room_id: Long,
    Room_Code: Option[String],
    Room_Capacity: Option[Long],
    Department_id: Long
) extends CoreData

object Room {
  implicit val roomSchema: Schema[Room] = Schema.derived
  val dc = new DoobieContext.Postgres(SnakeCase) // Literal naming scheme

  import dc.{SqlInfixInterpolator => _, _}

  val dbSchema = quote {
    querySchema[Room](
      "Room",
      _.Room_id -> "Room_id",
      _.Room_Code -> "Room_Code",
      _.Room_Capacity -> "Room_Capacity",
      _.Department_id -> "Department_id"
    )
  }

  def create(room: Room) =
    quote(
      dbSchema.insertValue(lift(room))
    )

  def read(id: Long) =
    quote(dbSchema.filter(_.Room_id == lift(id)))
  def update(room: Room) =
    quote(
      dbSchema.filter(_.Room_id == lift(room.Room_id)).updateValue(lift(room))
    )

  def delete(id: Long) =
    quote(
      dbSchema.filter(_.Room_id == lift(id)).delete
    )
}
