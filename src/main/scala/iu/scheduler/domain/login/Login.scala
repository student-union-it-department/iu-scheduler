package iu.scheduler.domain.login

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill.doobie.DoobieContext
import io.getquill.{EntityQuery, Literal, Quoted}
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema
@derive(tethysReader, tethysWriter)
final case class Login(
    Login_id: Long,
    Username: Option[String],
    Password: Option[String],
    AccessLevelVal: Option[String],
    Access_id: Long
) extends CoreData
object Login {
  implicit val loginSchema: Schema[Login] = Schema.derived

  val dc = new DoobieContext.Postgres(Literal) // Literal naming scheme

  import dc._

  val dbSchema: Quoted[EntityQuery[Login]] = quote {
    querySchema[Login](
      "Login",
      _.Login_id -> "Login_id",
      _.Username -> "Username",
      _.Password -> "Password",
      _.AccessLevelVal -> "AccessLevelVal",
      _.Access_id -> "Access_id"
    )
  }

  def create(login: Login) =
    quote(
      dbSchema.insertValue(lift(login))
    )

  def read(id: Long) =
    quote(
      dbSchema.filter(_.Login_id == lift(id))
    )

  def update(login: Login) =
    quote(
      dbSchema
        .filter(_.Login_id == lift(login.Login_id))
        .updateValue(lift(login))
    )

  def delete(id: Long) =
    quote(
      dbSchema.filter(_.Login_id == lift(id)).delete
    )

}
