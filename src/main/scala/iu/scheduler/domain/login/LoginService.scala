package iu.scheduler.domain.login

import cats.implicits.toFunctorOps
import cats.{Monad, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD

final class LoginService[F[_]: Monad](tx: ConnectionIO ~> F)
    extends CRUD[F, Login] {
  import Login.dc._
  override def create(id: Long, data: Login): F[Unit] =
    tx(run(Login.create(data))).void

  override def read(id: Long): F[Option[Login]] =
    tx(run(Login.read(id))).map(_.headOption)

  override def update(id: Long, data: Login): F[Unit] =
    tx(run(Login.update(data))).void

  override def delete(id: Long): F[Unit] =
    tx(run(Login.delete(id))).void
}
