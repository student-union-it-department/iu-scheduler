package iu.scheduler.domain.schedule

import cats.implicits.toFunctorOps
import cats.{Applicative, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD
final class ScheduleService[F[_]: Applicative](tx: ConnectionIO ~> F)
    extends CRUD[F, Schedule] {
  import Schedule.dc._
  override def create(id: Long, data: Schedule): F[Unit] =
    tx(
      run(
        quote(
          Schedule.dbSchema.insertValue(lift(data))
        )
      )
    ).void

  override def read(id: Long): F[Option[Schedule]] =
    tx(run(Schedule.read(id))).map(_.headOption)

  override def update(id: Long, data: Schedule): F[Unit] =
    tx(run(Schedule.update(data))).void

  override def delete(id: Long): F[Unit] =
    tx(run(Schedule.delete(id))).void
}
