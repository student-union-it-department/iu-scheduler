package iu.scheduler.domain.schedule

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import iu.scheduler.domain.CoreData
import iu.scheduler.utils.timeInstances._
import sttp.tapir.Schema

import java.time.LocalDateTime
@derive(tethysReader, tethysWriter)
final case class Schedule(
    tt_id: Long,
    tt_CourseName: Option[String],
    tt_DepartmentName: Option[String],
    tt_RoomCode: Option[String],
    tt_Day: Option[String],
    tt_StartTime: Option[LocalDateTime],
    tt_EndTime: Option[LocalDateTime],
    Department_id: Long,
    Course_id: Long,
    Room_id: Long,
    Faculty_id: Long,
    Schedules_id: Long
) extends CoreData
object Schedule {
  implicit val schedulesSchema: Schema[Schedule] = Schema.derived

  val dc = new DoobieContext.Postgres(SnakeCase) // Literal naming scheme

  import dc.{SqlInfixInterpolator => _, _}

  val dbSchema = quote {
    querySchema[Schedule](
      "Schedule",
      _.tt_id -> "tt_id",
      _.tt_CourseName -> "tt_CourseName",
      _.tt_DepartmentName -> "tt_DepartmentName",
      _.tt_RoomCode -> "tt_RoomCode",
      _.tt_Day -> "tt_Day",
      _.tt_StartTime -> "tt_StartTime",
      _.tt_EndTime -> "tt_EndTime",
      _.Department_id -> "Department_id",
      _.Course_id -> "Course_id",
      _.Room_id -> "Room_id",
      _.Faculty_id -> "Faculty_id",
      _.Schedules_id -> "Schedules_id"
    )
  }
  def create(schedule: Schedule) =
    quote(
      dbSchema.insertValue(lift(schedule))
    )

  def read(id: Long) =
    quote(
      dbSchema.filter(_.tt_id == lift(id))
    )

  def update(schedule: Schedule) =
    quote(
      dbSchema
        .filter(_.Schedules_id == lift(schedule.Schedules_id))
        .updateValue(lift(schedule))
    )

  def delete(id: Long) =
    quote(dbSchema.filter(_.Schedules_id == lift(id)).delete)

}
