package iu.scheduler.domain

trait CRUD[F[_], DATA <: CoreData] {
  def create(id: Long, data: DATA): F[Unit]
  def read(id: Long): F[Option[DATA]]
  def update(id: Long, data: DATA): F[Unit]
  def delete(id: Long): F[Unit]
}
