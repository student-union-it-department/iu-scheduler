package iu.scheduler.domain.department

import cats.implicits.toFunctorOps
import cats.{Monad, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD

final class DepartmentService[F[_]: Monad](tx: ConnectionIO ~> F)
    extends CRUD[F, Department] {

  import Department.dc._
  override def create(id: Long, data: Department): F[Unit] =
    tx(run(Department.queries.create(data))).void

  override def read(id: Long): F[Option[Department]] =
    tx(run(Department.queries.read(id))).map(_.headOption)

  override def update(id: Long, data: Department): F[Unit] =
    tx(run(Department.queries.update(data))).void

  override def delete(id: Long): F[Unit] =
    tx(run(Department.queries.delete(id))).void
}
