package iu.scheduler.domain.department

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill.doobie.DoobieContext
import io.getquill.{EntityQuery, Quoted, SnakeCase}
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema

@derive(tethysReader, tethysWriter)
final case class Department(
    Department_id: Long,
    Department_Name: Option[String]
) extends CoreData
object Department {
  implicit val departmentSchema: Schema[Department] = Schema.derived

  val dc = new DoobieContext.Postgres(SnakeCase) // Literal naming scheme

  import dc.{SqlInfixInterpolator => _, _}

  val dbSchema: Quoted[EntityQuery[Department]] = quote(
    querySchema[Department](
      "Department",
      _.Department_id -> "Department_id",
      _.Department_Name -> "Department_Name"
    )
  )

  object queries {
    def create(department: Department) =
      quote(
        dbSchema.insertValue(lift(department))
      )

    def read(id: Long) =
      quote(
        dbSchema.filter(_.Department_id == lift(id))
      )

    def update(department: Department) =
      quote(
        dbSchema
          .filter(_.Department_id == lift(department.Department_id))
          .updateValue(lift(department))
      )

    def delete(id: Long) =
      quote(
        dbSchema.filter(_.Department_id == lift(id)).delete
      )
  }

}
