package iu.scheduler.domain

import cats.effect.Sync
import org.http4s.HttpRoutes
import cats.implicits._
import org.http4s.dsl.Http4sDsl

import java.time.LocalDateTime
object Version {
  def helloWorldRoutes[F[_]: Sync]: HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] { case GET -> Root / "app" / "version" =>
      for {
        resp <- Ok(s"version for ${LocalDateTime.now.toString}")
      } yield resp
    }
  }
}

object Swagger {
  def swaggerRoutes[F[_]: Sync](yaml: String): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] { case GET -> Root / "app" / "swagger" =>
      for {
        resp <- Ok(s"$yaml")
      } yield resp
    }
  }
}
