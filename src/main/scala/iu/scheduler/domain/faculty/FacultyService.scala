package iu.scheduler.domain.faculty

import cats.implicits.toFunctorOps
import cats.{Monad, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD
final class FacultyService[F[_]: Monad](tx: ConnectionIO ~> F)
    extends CRUD[F, Faculty] {
  import Faculty.dc._
  override def create(id: Long, data: Faculty): F[Unit] =
    tx(run(Faculty.create(data))).void

  override def read(id: Long): F[Option[Faculty]] =
    tx(run(Faculty.read(id))).map(_.headOption)

  override def update(id: Long, data: Faculty): F[Unit] =
    tx(run(Faculty.update(data))).void

  override def delete(id: Long): F[Unit] =
    tx(run(Faculty.delete(id))).void
}
