package iu.scheduler.domain.faculty

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill._
import io.getquill.doobie.DoobieContext
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema
@derive(tethysReader, tethysWriter)
final case class Faculty(
    Faculty_id: Long,
    Faculty_Name: Option[String],
    Faculty_email: Option[String],
    Department_id: Long
) extends CoreData
object Faculty {
  implicit val facultySchema: Schema[Faculty] = Schema.derived
  val dc = new DoobieContext.Postgres(SnakeCase) // Literal naming scheme

  import dc.{SqlInfixInterpolator => _, _}

  val dbSchema = quote {
    querySchema[Faculty](
      "Faculty",
      _.Faculty_id -> "Faculty_id",
      _.Faculty_Name -> "Faculty_Name",
      _.Faculty_email -> "Faculty_email",
      _.Department_id -> "Department_id"
    )
  }

  def create(faculty: Faculty): Quoted[Insert[Faculty]] =
    quote(
      dbSchema.insertValue(lift(faculty))
    )

  def read(id: Long): Quoted[EntityQuery[Faculty]] =
    quote(
      dbSchema.filter(_.Faculty_id == lift(id))
    )

  def update(faculty: Faculty): Quoted[Update[Faculty]] =
    quote(
      dbSchema
        .filter(_.Faculty_id == lift(faculty.Faculty_id))
        .updateValue(lift(faculty))
    )

  def delete(id: Long): Quoted[Delete[Faculty]] =
    quote(
      dbSchema.filter(_.Faculty_id == lift(id)).delete
    )
}
