package iu.scheduler.domain.accesslvl

import cats.implicits.toFunctorOps
import cats.{Monad, ~>}
import doobie.ConnectionIO
import iu.scheduler.domain.CRUD
final class AccessLevelService[F[_]: Monad](tx: ConnectionIO ~> F)
    extends CRUD[F, AccessLevel] {

  import AccessLevel.dc._
  override def create(id: Long, data: AccessLevel): F[Unit] =
    tx(run(AccessLevel.queries.create(data))).void

  override def read(id: Long): F[Option[AccessLevel]] =
    tx(run(AccessLevel.queries.read(id))).map(_.headOption)

  override def update(id: Long, data: AccessLevel): F[Unit] =
    tx(run(AccessLevel.queries.update(data))).void

  override def delete(id: Long): F[Unit] =
    tx(run(AccessLevel.queries.delete(id))).void
}
