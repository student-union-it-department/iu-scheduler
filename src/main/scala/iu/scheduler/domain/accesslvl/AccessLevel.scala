package iu.scheduler.domain.accesslvl

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import io.getquill.Literal
import io.getquill.doobie.DoobieContext
import iu.scheduler.domain.CoreData
import sttp.tapir.Schema
@derive(tethysReader, tethysWriter)
final case class AccessLevel(
    Access_id: Long,
    Access_Name: Option[String],
    URL_Link: Option[String]
) extends CoreData
object AccessLevel {
  implicit val accessLevelSchema: Schema[AccessLevel] = Schema.derived
  val dc = new DoobieContext.Postgres(Literal) // Literal naming scheme
  import dc.{SqlInfixInterpolator => _, _}

  private val dbSchema = quote(
    querySchema[AccessLevel](
      "AccessLevel",
      _.Access_id -> "Access_id",
      _.Access_Name -> "Access_Name",
      _.URL_Link -> "URL_Link"
    )
  )

  object queries {
    def create(accessLevel: AccessLevel) =
      quote {
        dbSchema.insertValue(lift(accessLevel))
      }

    def read(id: Long) = quote(
      dbSchema.filter(_.Access_id == lift(id))
    )

    def update(accessLevel: AccessLevel) =
      quote(
        dbSchema
          .filter(_.Access_id == lift(accessLevel.Access_id))
          .updateValue(lift(accessLevel))
      )

    def delete(id: Long) =
      quote(
        dbSchema.filter(_.Access_id == lift(id)).delete
      )
  }

}
