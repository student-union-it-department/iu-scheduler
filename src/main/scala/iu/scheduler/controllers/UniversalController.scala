package iu.scheduler.controllers

import cats.MonadThrow
import cats.syntax.all._
import iu.scheduler.controllers.TapirJsonTethys.jsonBody
import iu.scheduler.domain.{CRUD, CoreData}
import sttp.tapir._
import tethys.{JsonReader, JsonWriter}

import scala.util.Random

final class UniversalController[F[
    _
]: MonadThrow, Data <: CoreData: Schema: JsonWriter: JsonReader](
    crud: CRUD[F, Data],
    dataPath: String
) {

  type ErrorInfo = String
  private val ep = endpoint
    .errorOut(plainBody[ErrorInfo])


  val createEndpoint = ep.post
    .in("api" / dataPath)
    .in(jsonBody[Data])
    .out(jsonBody[String])
    .description(s"creation of ${dataPath}")

  val updateEndpoint =
    ep.post
      .in("api" / dataPath / path[Long]("id"))
      .in(jsonBody[Data])
      .out(jsonBody[String])
      .description(s"Update of ${dataPath}")

  val readEndpoint = ep.get
    .in("api" / dataPath / path[Long]("id"))
    .out(jsonBody[Option[Data]])
    .description(s"Reading of ${dataPath}")

  val delete =
    ep.delete
      .in("api" / dataPath / path[Long]("id"))
      .out(jsonBody[String])
      .description(s"Deletion service for ${dataPath}")

  val services = Seq(
    createEndpoint.serverLogic[F] { data =>
      val id = Random.nextLong()
      crud
        .create(id, data)
        .attempt
        .map(_.leftMap(th => th.getMessage))
        .map(_.map(_ => id.toString))
    },
    updateEndpoint.serverLogic[F] { case (id, data) =>
      crud
        .update(id, data)
        .attempt
        .map(_.leftMap(th => th.getMessage))
        .map(_.map(_ => id.toString))
    },
    readEndpoint.serverLogic[F] { id =>
      crud
        .read(id)
        .attempt
        .map(_.leftMap(th => th.getMessage))

    },
    delete.serverLogic[F] { id =>
      crud
        .delete(id)
        .attempt
        .map(_.leftMap(th => th.getMessage))
        .map(_.map(_ => "OK"))
    }
  )

}
