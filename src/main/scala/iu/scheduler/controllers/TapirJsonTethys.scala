package iu.scheduler.controllers

import sttp.tapir.Codec.{JsonCodec, fromDecodeAndMeta}
import sttp.tapir.DecodeResult.{Error, Value}
import sttp.tapir._
import tethys.jackson._
import tethys._

trait TapirJsonTethys {

  implicit val bigDecimalReader: JsonReader[BigDecimal] =
    JsonReader.stringReader.map(number => BigDecimal(number))

  implicit val bigDecimalWriter: JsonWriter[BigDecimal] =
    JsonWriter.stringWriter.contramap[BigDecimal](_.toString())

  def jsonBody[T: JsonWriter: JsonReader: Schema]: EndpointIO.Body[String, T] =
    stringBodyUtf8AnyFormat(tethysCodec[T])

  implicit def tethysCodec[T: JsonReader: JsonWriter: Schema]: JsonCodec[T] =
    json(s =>
      s.jsonAs[T] match {
        case Left(readerError) => Error(readerError.getMessage, readerError)
        case Right(value)      => Value(value)
      }
    )(_.asJson)

  private def json[T: Schema](
      _rawDecode: String => DecodeResult[T]
  )(_encode: T => String): JsonCodec[T] = {
    anyStringCodec(CodecFormat.Json())(_rawDecode)(_encode)
  }

  private def anyStringCodec[T: Schema, CF <: CodecFormat](
      cf: CF
  )(
      _rawDecode: String => DecodeResult[T]
  )(_encode: T => String): Codec[String, T, CF] = {
    val isOptional = implicitly[Schema[T]].isOptional
    fromDecodeAndMeta(cf)({ (s: String) =>
      val toDecode = if (isOptional && s == "") "null" else s
      _rawDecode(toDecode)
    })(t => if (isOptional && t == None) "null" else _encode(t))
  }
}

object TapirJsonTethys extends TapirJsonTethys
