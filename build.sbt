name := "scheduler2023"

organization := "iu"

version := "0.1"


resolvers += Resolver.sonatypeRepo("snapshots")
resolvers += Resolver.bintrayIvyRepo("eed3si9n", "sbt-plugins")
resolvers += "Flyway" at "https://flywaydb.org/repo"

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

val CatsVersion = "3.4.8"
val CirceVersion = "0.14.1"
val CirceGenericExVersion = "0.14.1"
val CirceConfigVersion = "0.8.0"
val DoobieVersion = "0.13.4"
val EnumeratumCirceVersion = "1.7.2"
val H2Version = "1.4.200"
val http4sVersion = "0.23.19-RC3"
val KindProjectorVersion = "0.13.2"
val LogbackVersion = "1.2.6"
val Slf4jVersion = "1.7.30"
val ScalaCheckVersion = "1.15.4"
val ScalaTestVersion = "3.2.9"
val ScalaTestPlusVersion = "3.2.2.0"
val FlywayVersion = "7.15.0"
val TsecVersion = "0.2.1"
val tofuVersion = "0.11.1"
val tethysVersion = "0.26.0"
val enumeratumVersion = "1.7.2"
dependencyOverrides += "org.slf4j" % "slf4j-api" % Slf4jVersion

addCompilerPlugin(
  ("org.typelevel" %% "kind-projector" % KindProjectorVersion).cross(
    CrossVersion.full
  )
)
scalacOptions += "-Ymacro-annotations"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-effect" % CatsVersion,
  "com.github.pureconfig" %% "pureconfig" % "0.17.2",
  // Start with this one
  "org.tpolecat" %% "doobie-core" % "1.0.0-RC2",
  // And add any of these as needed
  "org.tpolecat" %% "doobie-h2" % "1.0.0-RC2", // H2 driver 1.4.200 + type mappings.
  "org.tpolecat" %% "doobie-hikari" % "1.0.0-RC2", // HikariCP transactor.
  "org.tpolecat" %% "doobie-postgres" % "1.0.0-RC2", // Postgres driver 42.3.1 + type mappings.
  "org.tpolecat" %% "doobie-specs2" % "1.0.0-RC2" % "test", // Specs2 support for typechecking statements.
  "org.tpolecat" %% "doobie-scalatest" % "1.0.0-RC2" % "test", // ScalaTest support for typechecking statements.
  "com.beachape" %% "enumeratum" % EnumeratumCirceVersion,
  "com.h2database" % "h2" % H2Version,
  "org.http4s" %% "http4s-ember-client" % http4sVersion,
  "org.http4s" %% "http4s-ember-server" % http4sVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % "1.4.0",
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "ch.qos.logback" % "logback-classic" % LogbackVersion,
  "org.flywaydb" % "flyway-core" % FlywayVersion,
  "org.scalacheck" %% "scalacheck" % ScalaCheckVersion % Test,
  "org.scalatest" %% "scalatest" % ScalaTestVersion % Test,
  "org.scalatestplus" %% "scalacheck-1-14" % ScalaTestPlusVersion % Test,
  // Authentication dependencies
  "com.tethys-json" %% "tethys-core" % tethysVersion,
  "com.tethys-json" %% "tethys-jackson" % tethysVersion,
  "com.tethys-json" %% "tethys-derivation" % tethysVersion,
  "com.beachape" %% "enumeratum" % enumeratumVersion,
  "tf.tofu" %% "derevo-tethys" % "0.13.0",
  "com.softwaremill.sttp.tapir" %% "tapir-core" % "1.2.10",
  "io.getquill" %% "quill-doobie" % "4.6.0",
  "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % "1.2.12",
  "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % "1.4.0"
)

enablePlugins(DockerPlugin, JavaAppPackaging, AshScriptPlugin, AssemblyPlugin)

fork in run := true

dockerExposedPorts ++= Seq(8080)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}